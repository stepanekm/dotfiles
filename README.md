# Dotfiles

## .vimrc

- Install vim-plug:
```sh
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

- Install nodejs (for Coc plugin)
- Install plugins
```vim
:PlugInstall
```
- Setup Coc plugin (mainly by [installing extensions](https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions#install-extensions) - for example, for python, run
```vim
:CocInstall coc-python
```
inside vim).
