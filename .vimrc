" Set 'nocompatible' to ward off unexpected things that your distro might
" have made, as well as sanely reset options when re-sourcing .vimrc
set nocompatible


" Attempt to determine the type of a file based on its name and possibly its
" contents. Use this to allow intelligent auto-indenting for each filetype,
" and for plugins that are filetype specific.
filetype indent plugin on

" Enable syntax highlighting
syntax on


" Plugins installed by Vim plug
call plug#begin('~/.vim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()


" Always show current position
set ruler


" Instead of failing a command because of unsaved changes, instead raise a
" dialogue asking if you wish to save changed files.
set confirm


" Use visual bell instead of beeping when doing something wrong
set visualbell


" And reset the terminal code for the visual bell. If visualbell is set, and
" this line is also included, vim will neither flash nor beep. If visualbell
" is unset, this does nothing.
set t_vb=


" Enable use of the mouse for all modes
set mouse=a


" Remove trailing whitespaces
fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    keepp %s/\s\+$//e
    call cursor(l, c)
endfun

autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()


" Set the command window height to 2 lines, to avoid many cases of having to
" "press <Enter> to continue"
set cmdheight=2


" Show relative line numbers
set number
set relativenumber


" Working with tabs
nnoremap <S-tab> :tabp<CR>
nnoremap <Tab>   :tabn<CR>
nnoremap <C-t>     :tabnew<CR>
inoremap <C-S-tab> <Esc>:tabp<CR>i
inoremap <C-tab>   <Esc>:tabn<CR>i
inoremap <C-t>     <Esc>:tabnew<CR>


" When scrolling, this number of lines above/below cursor will always be shown
set scrolloff=12


" Use spaces when tab is pressed
set tabstop=8
set softtabstop=0
set expandtab
set shiftwidth=2
set smarttab


" Autoindent
set autoindent


" Enhanced mode command line completition
set wildmenu
set wildmode=longest:full,full


" Better backspace
set backspace=indent,eol,start


" Arrows and h/l can move cursor to next/previous line
set whichwrap+=<,>,h,l


" Autocompletion of brackets and quotes
inoremap " ""<left>
inoremap ' ''<left>
inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O


" Ignore case when searching
set ignorecase

" When searching try to be smart about cases
set smartcase

" Highlight search results
set hlsearch

" Stop highlighting search results by pressing ESC
nnoremap <silent> <esc> :noh<cr><esc>

" Fix wierd behavior caused by remapping ESC
:nnoremap <esc>^[ <esc>^[

" Incremental search
set incsearch


" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8


" Linebreak on 500 characters
set lbr
set tw=500


" Always show status
:set laststatus=2


" Make ALT key shortcuts work
let c='a'
while c <= 'z'
  exec "set <A-".c.">=\e".c
  exec "imap \e".c." <A-".c.">"
  let c = nr2char(1+char2nr(c))
endw
set ttimeout ttimeoutlen=50


" Move a line of text using ALT+[jk]
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-k> :m '<-2<CR>gv=gv
vnoremap <A-j> :m '>+1<CR>gv=gv


" Ignore compiled files
set wildignore=*.o,*~,*.pyc
if has("win16") || has("win32")
    set wildignore+=.git\*,.hg\*,.svn\*
else
    set wildignore+=*/.git/*,*/.hg/*,*/.svn/*,*/.DS_Store
endif


" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
"Coc go to definition gd.
nmap <silent> gd <Plug>(coc-definition)

" Use K to show documentation or information about variable (for example data type).
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction
